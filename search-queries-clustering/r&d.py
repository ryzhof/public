# -*- coding: utf-8 -*-
"""
Created on Mon Aug  7 12:41:27 2017

@author: evgeny
"""
import pickle
import pandas as pd

import numpy as np
import sklearn.cluster
import distance

#words = "YOUR WORDS HERE".split(" ") #Replace this line
data = pd.read_pickle('data/data.pkl')

#data = data[:100]
words = data['query'][:100].values #So that indexing with a list will work
#words = np.asarray(words) #So that indexing with a list will work
lev_similarity = -1*np.array([[distance.levenshtein(w1,w2) for w1 in words] for w2 in words])

affprop = sklearn.cluster.AffinityPropagation(affinity="precomputed", damping=0.5)
affprop.fit(lev_similarity)
for cluster_id in np.unique(affprop.labels_):
    exemplar = words[affprop.cluster_centers_indices_[cluster_id]]
    cluster = np.unique(words[np.nonzero(affprop.labels_==cluster_id)])
    cluster_str = ", ".join(cluster)
    print(" - *%s:* %s" % (exemplar, cluster_str))
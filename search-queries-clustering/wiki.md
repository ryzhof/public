## clustering

[Clustering text with python](https://stats.stackexchange.com/questions/65929/clustering-text-with-python)

[Обзор алгоритмов кластеризации данных](https://habrahabr.ru/post/101338/

[Кластеризация поисковых запросов в Python](http://coderhs.com/archive/cluster-search-queries-Python)


## similarity

[Space and Cosine Similarity measures for Text Document Clustering]  (http://www.ijert.org/view-pdf/2373/space-and-cosine-similarity-measures-for-text-document-clustering)

[Python: Semantic similarity score for Strings duplicate]  (https://stackoverflow.com/questions/17022691/python-semantic-similarity-score-for-strings)

[search similar meaning phrases with nltk] (https://stackoverflow.com/questions/22452713/search-similar-meaning-phrases-with-nltk)

## recommendations

[Построение рекомендательной системы на Python] (https://www.slideshare.net/AvitoTech/python-avito)

[Введение в анализ текстовой информации с помощью Python и методов машинного обучения]  (https://habrahabr.ru/post/205360/)

https://turi.com/learn/userguide/install.html
Открытый курс машинного обучения. Тема 6. Построение и отбор признаков  https://habrahabr.ru/company/ods/blog/325422/
https://owlweb.ru/wp-content/uploads/2017/06/a.myuller-s.gvido-vvedenie-v-mashinnoe-obuchenie-s-pomoshhyu-python.-rukovodstvo-dlya-specialistov-po-rabote-s-dannymi-2017.compressed-1.pdf
Recommendation systems: Principles, methods and evaluation http://www.sciencedirect.com/science/article/pii/S1110866515000341
Препарируем t-SNE https://habrahabr.ru/post/267041/

https://pdfs.semanticscholar.org/599e/beef9c9d92224bc5969f3e8e8c45bff3b072.pdf
https://www.slideshare.net/AvitoTech/python-avito
Нечёткий поиск в тексте и словаре https://habrahabr.ru/post/114997/
https://ep2015.europython.eu/conference/talks/speeding-up-search-with-locality-sensitive-hashing
https://habrahabr.ru/company/ivi/blog/232843/
Коллаборативная фильтрация К. В. Воронцов http://www.machinelearning.ru/wiki/images/archive/9/95/20140413184117!Voron-ML-CF.pdf
Collaborative and Content-based Filtering for Item Recommendation on Social Bookmarking Websites  http://ceur-ws.org/Vol-532/paper2.pdf
Data Mining: Finding Similar Items and Users https://alexn.org/blog/2012/01/16/cosine-similarity-euclidean-distance.html
Learning to rank with Python scikit-learn https://medium.com/towards-data-science/learning-to-rank-with-python-scikit-learn-327a5cfd81f
Predicting the Relevance of Search Results for E-Commerce Systems https://www.researchgate.net/publication/286219675_Predicting_the_Relevance_of_Search_Results_for_E-Commerce_Systems
Developing a search algorithm https://softwareengineering.stackexchange.com/questions/166702/developing-a-search-algorithm
https://www.buyboxexperts.com/amazons-product-ranking-algorithm/

Document Clustering with Python http://brandonrose.org/clustering
https://www.knime.com/knime-applications/churn-prediction
https://mlichtenberg.wordpress.com/2013/09/09/comparing-documents-with-bayes-classification-term-frequencyinverse-document-frequency-and-levenshtein-distance-algorithms/
https://codereview.stackexchange.com/questions/151908/sentences-clustering-affinity-propagation-cosine-similarity-python-sciki
https://stats.stackexchange.com/questions/123060/clustering-a-long-list-of-strings-words-into-similarity-groups

https://docs.google.com/document/d/1K7jATh-T1H9bg9YeAKPdXD41ZCWAlE--9yxB3mivmJc/edit#heading=h.5zmmriwwn8sg

## libs
[turi](https://turi.com/learn/gallery/notebooks/click_through_rate_prediction_intro.html)

## models
https://stats.stackexchange.com/questions/134735/what-is-a-good-model-for-revenue-managment-price-optimization-problem?rq=1
A Predictive Model for Advertiser Value-Per-Click in
Sponsored Search http://cs.brown.edu/~sodomka/resources/papers/2013-www.pdf
A Logistic Regression Approach to Ad Click Prediction https://pdfs.semanticscholar.org/d1c9/740a8a642505d9caf813a711023ea928d599.pdf
Predict the Click-Through Rate and Average Cost Per Click for Keywords Using Machine Learning Methodologies  http://ieomsociety.org/ieomdetroit/pdfs/246.pdf
